package manka.igor.perform.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.domain.exception.NetErrorException;
import manka.igor.perform.domain.interactor.news.GetNewsInteractor;
import manka.igor.perform.domain.interactor.news.impl.GetNewsInteractorImpl;
import manka.igor.perform.presentation.ui.news.model.News;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetNewsInteractorTest {

    @Mock
    PerformRepository performRepository;

    @Mock
    GetNewsInteractor.Callback callback;

    GetNewsInteractor getNewsInteractor;

    @Before
    public void setUp() {
        getNewsInteractor = new GetNewsInteractorImpl(performRepository);
    }

    @Test
    public void testExecute() {
        News mockNews = mock(News.class);
        Observable<News> mockObservable = Observable.just(mockNews);
        when(performRepository.getNews()).thenReturn(mockObservable);

        getNewsInteractor.execute(callback);
        verify(callback, never()).error(any(NetErrorException.class));
        verify(callback, never()).failure(any(NetErrorException.class));
        verify(callback, only()).success(eq(mockNews));
    }

}
