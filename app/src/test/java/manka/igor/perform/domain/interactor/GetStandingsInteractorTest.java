package manka.igor.perform.domain.interactor;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Observable;
import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.domain.exception.NetErrorException;
import manka.igor.perform.domain.interactor.standings.GetStandingsInteractor;
import manka.igor.perform.domain.interactor.standings.impl.GetStandingsInteractorImpl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.only;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GetStandingsInteractorTest {

    @Mock
    PerformRepository performRepository;

    @Mock
    GetStandingsInteractor.Callback callback;

    GetStandingsInteractor getStandingsInteractor;

    @Before
    public void setUp() {
        getStandingsInteractor = new GetStandingsInteractorImpl(performRepository);
    }

    @Test
    public void testExecute() {
        StandingsResponse mockStandingsResponse = mock(StandingsResponse.class);
        Observable<StandingsResponse> mockObservable = Observable.just(mockStandingsResponse);
        when(performRepository.getStandings()).thenReturn(mockObservable);

        getStandingsInteractor.execute(callback);
        verify(callback, never()).error(any(NetErrorException.class));
        verify(callback, never()).failure(any(NetErrorException.class));
        verify(callback, only()).success(eq(mockStandingsResponse));
    }


}
