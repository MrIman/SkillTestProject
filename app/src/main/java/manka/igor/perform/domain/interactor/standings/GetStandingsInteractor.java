package manka.igor.perform.domain.interactor.standings;

import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.domain.interactor.base.NetCallback;
import manka.igor.perform.domain.interactor.base.NetInteractor;

public interface GetStandingsInteractor extends NetInteractor {

    void execute(GetStandingsInteractor.Callback callback);

    interface Callback extends NetCallback {

        void success(StandingsResponse standingsResponse);
    }
}
