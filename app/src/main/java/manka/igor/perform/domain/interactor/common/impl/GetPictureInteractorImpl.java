package manka.igor.perform.domain.interactor.common.impl;

import android.util.Log;

import javax.inject.Inject;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.domain.interactor.common.GetPictureInteractor;
import manka.igor.perform.presentation.ui.news.model.News;


public class GetPictureInteractorImpl implements GetPictureInteractor {

    private final PerformRepository performRepository;

    @Inject
    public GetPictureInteractorImpl(PerformRepository performRepository) {
        this.performRepository = performRepository;
    }

    @Override
    public void execute(News news, final GetPictureInteractor.Callback callback) {
        Observer observer = new Observer<News>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {

            }

            @Override
            public void onNext(@NonNull News newsWithPicture) {
                callback.success(newsWithPicture);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.d("Error", e.getMessage());
            }

            @Override
            public void onComplete() {

            }
        };

        performRepository.getPictureJPG(news).subscribe(observer);
    }
}
