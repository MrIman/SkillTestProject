package manka.igor.perform.domain.interactor.scores.impl;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import manka.igor.perform.data.net.entity.ScoresResponse;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.domain.interactor.base.NetObserver;
import manka.igor.perform.domain.interactor.base.impl.NetInteractorImpl;
import manka.igor.perform.domain.interactor.scores.GetScoresInteractor;

public class GetScoresInteractorImpl extends NetInteractorImpl implements GetScoresInteractor {


    private static final int REQUEST_REFRESH_SECONDS = 30;
    private final PerformRepository performRepository;

    @Inject
    public GetScoresInteractorImpl(PerformRepository performRepository) {

        this.performRepository = performRepository;
    }

    @Override
    public void execute(final Callback callback) {
        netObserver = new NetObserver<ScoresResponse>(callback) {
            @Override
            public void onNext(@NonNull ScoresResponse scoresResponse) {
                callback.success(scoresResponse);
            }
        };

        Observable<Long> interval = Observable.interval(REQUEST_REFRESH_SECONDS, TimeUnit.SECONDS);
        interval.flatMap(new Function<Long, ObservableSource<?>>() {
            @Override
            public ObservableSource<?> apply(@NonNull Long aLong) throws Exception {
                return performRepository.getScores();
            }
        }).subscribe(netObserver);
    }
}
