package manka.igor.perform.domain;


import io.reactivex.Observable;
import manka.igor.perform.data.net.entity.ScoresResponse;
import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.presentation.ui.news.model.News;

public interface PerformRepository {

    Observable<News> getNews();

    Observable<News> getPictureJPG(News url);

    Observable<ScoresResponse> getScores();

    Observable<StandingsResponse> getStandings();
}
