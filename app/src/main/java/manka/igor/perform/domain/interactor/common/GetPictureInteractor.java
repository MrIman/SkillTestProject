package manka.igor.perform.domain.interactor.common;

import manka.igor.perform.domain.interactor.base.NetCallback;
import manka.igor.perform.presentation.ui.news.model.News;

public interface GetPictureInteractor {

    void execute(News news, GetPictureInteractor.Callback callback);

    interface Callback extends NetCallback {
        void success(News newsWithPicture);
    }
}
