package manka.igor.perform.domain.interactor.news.impl;


import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.domain.interactor.base.NetObserver;
import manka.igor.perform.domain.interactor.base.impl.NetInteractorImpl;
import manka.igor.perform.domain.interactor.news.GetNewsInteractor;
import manka.igor.perform.presentation.ui.news.model.News;

public class GetNewsInteractorImpl extends NetInteractorImpl implements GetNewsInteractor {

    private final PerformRepository performRepository;

    @Inject
    public GetNewsInteractorImpl(PerformRepository performRepository) {
        this.performRepository = performRepository;
    }

    @Override
    public void execute(final Callback callback) {
        netObserver = new NetObserver<News>(callback) {
            @Override
            public void onNext(@NonNull News news) {
                callback.success(news);
            }
        };

        performRepository.getNews().subscribe(netObserver);
    }
}
