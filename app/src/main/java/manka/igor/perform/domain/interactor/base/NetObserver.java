package manka.igor.perform.domain.interactor.base;

import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import lombok.Getter;
import manka.igor.perform.domain.exception.NetErrorException;
import retrofit2.HttpException;

public abstract class NetObserver<T> implements Observer<T> {

    private NetCallback callback;

    @Getter
    private Disposable disposable;

    public NetObserver(NetCallback callback) {
        this.callback = callback;
    }

    @Override
    public void onSubscribe(Disposable disposable) {
        this.disposable = disposable;
    }

    @Override
    public void onError(Throwable throwable) {
        if (throwable instanceof HttpException) {
            callback.failure(NetErrorException.httpError((HttpException) throwable));

            return;
        }
        if (throwable instanceof IOException) {
            if (callback != null) {
                callback.error(NetErrorException.networkError((IOException) throwable));
            }

            return;
        }

        if (callback != null) {
            callback.error(NetErrorException.unexpectedError(throwable));
        }
    }

    @Override
    public void onComplete() {
    }
}
