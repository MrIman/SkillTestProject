package manka.igor.perform.domain.interactor.scores;

import manka.igor.perform.data.net.entity.ScoresResponse;
import manka.igor.perform.domain.interactor.base.NetCallback;
import manka.igor.perform.domain.interactor.base.NetInteractor;

public interface GetScoresInteractor extends NetInteractor {

    void execute(GetScoresInteractor.Callback callback);

    interface Callback extends NetCallback {

        void success(ScoresResponse scoresResponse);
    }
}
