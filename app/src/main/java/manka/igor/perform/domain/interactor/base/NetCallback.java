package manka.igor.perform.domain.interactor.base;

public interface NetCallback {

    void failure(Exception exception);

    void error(Exception exception);
}
