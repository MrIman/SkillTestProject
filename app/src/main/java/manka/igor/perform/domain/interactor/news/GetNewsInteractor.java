package manka.igor.perform.domain.interactor.news;

import manka.igor.perform.domain.interactor.base.NetCallback;
import manka.igor.perform.domain.interactor.base.NetInteractor;
import manka.igor.perform.presentation.ui.news.model.News;

public interface GetNewsInteractor extends NetInteractor {

    void execute(GetNewsInteractor.Callback callback);

    interface Callback extends NetCallback {
        void success(News news);
    }
}
