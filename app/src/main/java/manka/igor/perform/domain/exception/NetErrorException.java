package manka.igor.perform.domain.exception;

import java.io.IOException;

import lombok.Data;
import retrofit2.HttpException;
import retrofit2.Response;

@Data
public class NetErrorException extends RuntimeException {

    private final String message;
    private final Response response;
    private final ErrorKind errorKind;

    private NetErrorException(String message, Response response, ErrorKind errorKind) {
        super(message);

        this.message = message;
        this.response = response;
        this.errorKind = errorKind;
    }

    public static NetErrorException httpError(HttpException ex) {
        Response response = ex.response();
        String message = "(" + response.code() + ") " + response.message();

        return new NetErrorException(message, response, ErrorKind.HTTP);
    }

    public static NetErrorException networkError(IOException ex) {
        return new NetErrorException(ex.getMessage(), null, ErrorKind.NETWORK);
    }

    public static NetErrorException unexpectedError(Throwable throwable) {
        return new NetErrorException(throwable.getMessage(), null, ErrorKind.UNEXPECTED);
    }

    public enum ErrorKind {
        HTTP,
        NETWORK,
        UNEXPECTED
    }
}
