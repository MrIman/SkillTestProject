package manka.igor.perform.domain.interactor.standings.impl;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.domain.interactor.base.NetObserver;
import manka.igor.perform.domain.interactor.base.impl.NetInteractorImpl;
import manka.igor.perform.domain.interactor.standings.GetStandingsInteractor;


public class GetStandingsInteractorImpl extends NetInteractorImpl implements GetStandingsInteractor {

    private PerformRepository performRepository;

    @Inject
    public GetStandingsInteractorImpl(PerformRepository performRepository) {

        this.performRepository = performRepository;
    }

    @Override
    public void execute(final Callback callback) {
        netObserver = new NetObserver<StandingsResponse>(callback) {
            @Override
            public void onNext(@NonNull StandingsResponse standingsResponse) {
                callback.success(standingsResponse);
            }
        };
        performRepository.getStandings().subscribe(netObserver);
    }
}
