package manka.igor.perform.domain.interactor.base.impl;

import lombok.Setter;
import manka.igor.perform.domain.interactor.base.NetInteractor;
import manka.igor.perform.domain.interactor.base.NetObserver;

public class NetInteractorImpl implements NetInteractor {

    @Setter
    protected NetObserver netObserver;

    @Override
    public void dispose() {
        if (netObserver == null || netObserver.getDisposable() == null) {
            return;
        }

        netObserver.getDisposable().dispose();
    }
}
