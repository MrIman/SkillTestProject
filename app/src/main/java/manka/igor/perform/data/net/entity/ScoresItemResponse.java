package manka.igor.perform.data.net.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import lombok.Data;

@Data
@Root(name = "match", strict = false)
public class ScoresItemResponse {

    @Attribute(name = "team_A_name")
    private String firstTeamName;

    @Attribute(name = "team_B_name")
    private String secondTeamName;

    @Attribute(name = "fs_A")
    private int firstTeamScore;

    @Attribute(name = "fs_B")
    private int secondTeamScore;
}
