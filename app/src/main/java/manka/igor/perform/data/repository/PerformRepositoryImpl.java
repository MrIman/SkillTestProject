package manka.igor.perform.data.repository;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.schedulers.Schedulers;
import manka.igor.perform.data.net.entity.ScoresResponse;
import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.data.net.service.PerformNetService;
import manka.igor.perform.domain.PerformRepository;
import manka.igor.perform.presentation.ui.news.model.News;

public class PerformRepositoryImpl implements PerformRepository {

    private final PerformNetService performNetService;

    @Inject
    public PerformRepositoryImpl(PerformNetService performNetService) {

        this.performNetService = performNetService;
    }

    @Override
    public Observable<News> getNews() {
        return performNetService.getNews()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<News> getPictureJPG(final News news) {
        final News newsWithPicture = news;

        return Observable.create(new ObservableOnSubscribe<News>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<News> e) throws Exception {
                for (int iterator = 0; iterator < news.getChannel().getNewsItemList().size(); iterator++) {
                    try {
                        URL url = new URL(news.getChannel().getNewsItemList().get(iterator).getEnclosure().getUrl());
                        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                        connection.setDoInput(true);
                        connection.connect();
                        InputStream input = connection.getInputStream();
                        Bitmap photo = BitmapFactory.decodeStream(input);
                        newsWithPicture.getChannel().getNewsItemList().get(iterator).setBitmap(photo);
                    } catch (IOException exception) {
                        exception.printStackTrace();
                        e.onError(exception);
                    }
                }
                e.onNext(newsWithPicture);
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ScoresResponse> getScores() {
        return performNetService.getScores()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<StandingsResponse> getStandings() {
        return performNetService.getStandings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


}
