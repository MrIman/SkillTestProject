package manka.igor.perform.data.net.service;


import io.reactivex.Observable;
import manka.igor.perform.data.net.entity.ScoresResponse;
import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.presentation.ui.news.model.News;
import retrofit2.http.GET;

public interface PerformNetService {

    @GET("utilities/interviews/techtest/latestnews.xml")
    Observable<News> getNews();

    @GET("/utilities/interviews/techtest/scores.xml")
    Observable<ScoresResponse> getScores();

    @GET("/utilities/interviews/techtest/standings.xml")
    Observable<StandingsResponse> getStandings();
}
