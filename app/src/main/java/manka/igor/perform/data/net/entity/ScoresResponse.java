package manka.igor.perform.data.net.entity;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import lombok.Data;

@Data
@Root(name = "gsmrs", strict = false)
public class ScoresResponse {

    @Element(name = "competition")
    Competition competition;

    @Data
    @Root(name = "competition", strict = false)
    public static class Competition {
        @Element(name = "season")
        Season season;
    }

    @Data
    @Root(name = "season", strict = false)
    public static class Season {
        @Element(name = "round")
        Round round;
    }

    @Data
    @Root(name = "round", strict = false)
    public static class Round {
        @ElementList(name = "group", inline = true, required = false)
        List<Group> groups;
    }

    @Data
    @Root(name = "group", strict = false)
    public static class Group {
        @ElementList(name = "match", inline = true, required = false)
        List<ScoresItemResponse> scoresItemResponseList;
    }
}
