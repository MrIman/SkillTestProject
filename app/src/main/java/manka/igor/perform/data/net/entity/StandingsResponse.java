package manka.igor.perform.data.net.entity;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import lombok.Data;

@Data
@Root(name = "gsmrs", strict = false)
public class StandingsResponse {

    @Element(name = "competition")
    Competition competition;

    @Data
    @Root(name = "competition", strict = false)
    public static class Competition {
        @Element(name = "season")
        Season season;
    }

    @Data
    @Root(name = "season", strict = false)
    public static class Season {
        @Element(name = "round")
        Round round;
    }

    @Data
    @Root(name = "round", strict = false)
    public static class Round {
        @Element(name = "resultstable")
        Results results;
    }

    @Data
    @Root(name = "resultstable", strict = false)
    public static class Results {
        @ElementList(name = "ranking", required = false, inline = true)
        List<Ranking> rankingList;
    }

    @Data
    @Root(name = "ranking", strict = false)
    public static class Ranking {

        @Attribute(name = "rank")
        int rank;

        @Attribute(name = "club_name")
        String clubName;

        @Attribute(name = "matches_total")
        int matches;

        @Attribute(name = "matches_won")
        int matchesWon;

        @Attribute(name = "matches_draw")
        int matchesDraw;

        @Attribute(name = "matches_lost")
        int matchesLost;

        @Attribute(name = "points")
        int points;

    }


}
