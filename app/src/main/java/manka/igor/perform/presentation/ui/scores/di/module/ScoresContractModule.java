package manka.igor.perform.presentation.ui.scores.di.module;

import dagger.Module;
import dagger.Provides;
import manka.igor.perform.presentation.di.scope.PerActivity;
import manka.igor.perform.presentation.ui.scores.contract.ScoresContract;

@Module
public class ScoresContractModule {

    private ScoresContract.View view;

    public ScoresContractModule(ScoresContract.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    ScoresContract.View providesView() {
        return view;
    }


}
