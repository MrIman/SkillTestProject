package manka.igor.perform.presentation.ui.news.di.component;

import dagger.Component;
import manka.igor.perform.presentation.di.component.DataComponent;
import manka.igor.perform.presentation.di.scope.PerActivity;
import manka.igor.perform.presentation.ui.news.di.module.NewsContractModule;
import manka.igor.perform.presentation.ui.news.view.fragment.NewsFragment;

@PerActivity
@Component(dependencies = DataComponent.class,
        modules = NewsContractModule.class)
public interface NewsComponent {

    void inject(NewsFragment fragment);
}
