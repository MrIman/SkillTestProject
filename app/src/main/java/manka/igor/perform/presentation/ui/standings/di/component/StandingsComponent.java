package manka.igor.perform.presentation.ui.standings.di.component;

import dagger.Component;
import manka.igor.perform.presentation.di.component.DataComponent;
import manka.igor.perform.presentation.di.scope.PerActivity;
import manka.igor.perform.presentation.ui.standings.di.module.StandingsContractModule;
import manka.igor.perform.presentation.ui.standings.view.fragment.StandingsFragment;

@PerActivity
@Component(dependencies = DataComponent.class,
        modules = StandingsContractModule.class)
public interface StandingsComponent {

    void inject(StandingsFragment fragment);
}
