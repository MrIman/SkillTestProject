package manka.igor.perform.presentation.ui.news.view.fragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.PerformApplication;
import manka.igor.perform.presentation.ui.base.fragment.BaseFragment;
import manka.igor.perform.presentation.ui.main.PerformActivity;
import manka.igor.perform.presentation.ui.news.contract.NewsContract;
import manka.igor.perform.presentation.ui.news.di.component.DaggerNewsComponent;
import manka.igor.perform.presentation.ui.news.di.module.NewsContractModule;
import manka.igor.perform.presentation.ui.news.model.News;
import manka.igor.perform.presentation.ui.news.presenter.NewsPresenter;
import manka.igor.perform.presentation.ui.news.view.adapter.NewsRecyclerViewAdapter;

public class NewsFragment extends BaseFragment implements NewsContract.View {

    public static final int layout_id = R.layout.fragment_news;

    @Inject
    NewsPresenter newsPresenter;

    @BindView(R.id.news_recyclerView)
    RecyclerView newsRecyclerView;

    private NewsRecyclerViewAdapter newsRecyclerViewAdapter;

    public NewsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(layout_id, container, false);
        ButterKnife.bind(this, view);
        setUpInjection();
        setUpView();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpView();
    }

    @Override
    public void onPause() {
        super.onPause();
        newsPresenter.dispose();
    }

    @Override
    public void setUpInjection() {
        DaggerNewsComponent.builder()
                .dataComponent(((PerformApplication) getActivity().getApplication()).getDataComponent())
                .newsContractModule(new NewsContractModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onSuccessGetNews(News news) {
        newsPresenter.fetchPicture(news);
    }

    @Override
    public void onSuccessGetPicture(News newsWithPicture) {
        setUpRecyclerView(newsWithPicture);
        try {
            ((PerformActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
        } catch (Exception ex) {
            Log.d("Error", ex.getMessage());
        }
    }

    private void setUpView() {
        try {
            ((PerformActivity) getActivity()).getProgressBar().setVisibility(View.VISIBLE);
        } catch (Exception ex) {
            Log.d("Error", ex.getMessage());
        }
        newsPresenter.fetchNewsData();
    }

    private void setUpRecyclerView(News news) {
        newsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        newsRecyclerViewAdapter = new NewsRecyclerViewAdapter(news);
        newsRecyclerView.setAdapter(newsRecyclerViewAdapter);
        newsRecyclerViewAdapter.notifyDataSetChanged();
    }
}
