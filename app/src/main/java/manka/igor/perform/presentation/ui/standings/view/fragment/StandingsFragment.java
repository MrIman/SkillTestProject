package manka.igor.perform.presentation.ui.standings.view.fragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.PerformApplication;
import manka.igor.perform.presentation.ui.base.fragment.BaseFragment;
import manka.igor.perform.presentation.ui.main.PerformActivity;
import manka.igor.perform.presentation.ui.standings.contract.StandingsContract;
import manka.igor.perform.presentation.ui.standings.di.component.DaggerStandingsComponent;
import manka.igor.perform.presentation.ui.standings.di.module.StandingsContractModule;
import manka.igor.perform.presentation.ui.standings.model.Standings;
import manka.igor.perform.presentation.ui.standings.presenter.StandingsPresenter;
import manka.igor.perform.presentation.ui.standings.view.adapter.StandingsRecyclerViewAdapter;

public class StandingsFragment extends BaseFragment implements StandingsContract.View {

    public static final int layout_id = R.layout.fragment_standings;

    @Inject
    StandingsPresenter standingsPresenter;

    @BindView(R.id.standings_recyclerView)
    RecyclerView standingsRecyclerView;

    private StandingsRecyclerViewAdapter standingsRecyclerViewAdapter;

    public StandingsFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(layout_id, container, false);
        ButterKnife.bind(this, view);
        setUpInjection();
        setUpView();
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        standingsPresenter.dispose();
    }

    @Override
    public void onResume() {
        super.onResume();
        setUpView();
    }

    @Override
    public void setUpInjection() {
        DaggerStandingsComponent.builder()
                .dataComponent(((PerformApplication) getActivity().getApplication()).getDataComponent())
                .standingsContractModule(new StandingsContractModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onFetchStandingsDataSuccess(List<Standings> standingsList) {
        setUpRecyclerView(standingsList);
        try {
            ((PerformActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
        } catch (Exception ex) {
            Log.d("Error", ex.getMessage());
        }
    }

    private void setUpView() {
        ((PerformActivity) getActivity()).getProgressBar().setVisibility(View.VISIBLE);
        standingsPresenter.fetchStandingsData();
    }


    private void setUpRecyclerView(List<Standings> standingsList) {
        standingsRecyclerView.setNestedScrollingEnabled(false);
        List<Standings> sortedStandings = sortStandings(standingsList);
        standingsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        standingsRecyclerViewAdapter = new StandingsRecyclerViewAdapter(sortedStandings);
        standingsRecyclerView.setAdapter(standingsRecyclerViewAdapter);
        standingsRecyclerViewAdapter.notifyDataSetChanged();
    }

    private List<Standings> sortStandings(List<Standings> standingsList) {
        Collections.sort(standingsList, new Comparator<Standings>() {
            @Override
            public int compare(Standings lhs, Standings rhs) {
                return lhs.getRank() < rhs.getRank() ? -1 : (lhs.getRank() < rhs.getRank()) ? 1 : 0;
            }
        });
        return standingsList;
    }

}
