package manka.igor.perform.presentation.di.component;

import javax.inject.Singleton;

import dagger.Component;
import manka.igor.perform.domain.interactor.common.GetPictureInteractor;
import manka.igor.perform.domain.interactor.news.GetNewsInteractor;
import manka.igor.perform.domain.interactor.scores.GetScoresInteractor;
import manka.igor.perform.domain.interactor.standings.GetStandingsInteractor;
import manka.igor.perform.presentation.di.module.ApplicationModule;
import manka.igor.perform.presentation.di.module.InteractorModule;
import manka.igor.perform.presentation.di.module.NetModule;
import manka.igor.perform.presentation.di.module.RepositoryModule;

@Singleton
@Component(modules = {ApplicationModule.class,
        NetModule.class,
        InteractorModule.class,
        RepositoryModule.class})
public interface DataComponent {

    GetNewsInteractor getNewsInteractor();

    GetPictureInteractor getPictureInteractor();

    GetScoresInteractor getScoresInteractor();

    GetStandingsInteractor getStandingsInteractor();
}

