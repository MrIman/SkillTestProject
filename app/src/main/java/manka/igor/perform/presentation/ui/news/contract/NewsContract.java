package manka.igor.perform.presentation.ui.news.contract;

import manka.igor.perform.presentation.ui.news.model.News;

public interface NewsContract {

    interface View {

        void onSuccessGetNews(News news);

        void onSuccessGetPicture(News newsWithPicture);
    }

    interface Presenter {

        void fetchNewsData();

        void fetchPicture(News news);
    }
}
