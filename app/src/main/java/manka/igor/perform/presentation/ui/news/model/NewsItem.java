package manka.igor.perform.presentation.ui.news.model;

import android.graphics.Bitmap;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import lombok.Data;

@Data
@Root(name = "item", strict = false)
public class NewsItem {

    @Element(name = "title")
    private String title;

    @Element(name = "link")
    private String link;

    @Element(name = "description")
    private String description;

    @Element(name = "enclosure")
    private Enclosure enclosure;

    @Element(name = "guid")
    private String guid;

    @Element(name = "pubDate")
    private String pubDate;

    @Element(name = "category")
    private String category;
    private Bitmap bitmap;

    @Data
    public static class Enclosure {

        @Attribute(name = "url")
        private String url;

        @Attribute(name = "length")
        private long length;

        @Attribute(name = "type")
        private String type;
    }

}
