package manka.igor.perform.presentation.ui.webview.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebView;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.Constants;
import manka.igor.perform.presentation.ui.webview.CustomWebView;

public class WebViewActivity extends AppCompatActivity {

    @BindView(R.id.webView)
    WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpExtras();
    }

    private void setUpExtras() {
        if (getIntent().getExtras() != null) {
            String url = getIntent().getStringExtra(Constants.WEB_VIEW_EXTRA);
            setUpView(url);
        }
    }

    private void setUpView(String url) {
        if (!url.equals(null)) {
            webView.setWebViewClient(new CustomWebView());
            webView.getSettings().setLoadsImagesAutomatically(true);
            webView.getSettings().setJavaScriptEnabled(true);
            webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webView.loadUrl(url);
        }
    }
}
