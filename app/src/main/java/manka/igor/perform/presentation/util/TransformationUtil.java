package manka.igor.perform.presentation.util;

import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

public class TransformationUtil {

    public static void rotateView(ImageView view, int rotateStart, int rotateEnd) {

        RotateAnimation rotateAnim = new RotateAnimation(rotateStart, rotateEnd, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        rotateAnim.setDuration(500);
        rotateAnim.setInterpolator(new LinearInterpolator());
        rotateAnim.setFillAfter(true);
        rotateAnim.setFillEnabled(true);
        view.setAnimation(rotateAnim);
    }
}
