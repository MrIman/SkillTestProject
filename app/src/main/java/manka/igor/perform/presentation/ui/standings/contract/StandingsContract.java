package manka.igor.perform.presentation.ui.standings.contract;

import java.util.List;

import manka.igor.perform.presentation.ui.standings.model.Standings;

public interface StandingsContract {

    interface View {

        void onFetchStandingsDataSuccess(List<Standings> standingsList);
    }

    interface Presenter {

        void fetchStandingsData();
    }
}
