package manka.igor.perform.presentation.ui.standings.presenter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import manka.igor.perform.data.net.entity.StandingsResponse;
import manka.igor.perform.domain.interactor.standings.GetStandingsInteractor;
import manka.igor.perform.presentation.ui.standings.contract.StandingsContract;
import manka.igor.perform.presentation.ui.standings.model.Standings;

public class StandingsPresenter implements StandingsContract.Presenter {

    private StandingsContract.View view;
    private GetStandingsInteractor getStandingsInteractor;

    @Inject
    public StandingsPresenter(StandingsContract.View view,
                              GetStandingsInteractor getStandingsInteractor) {

        this.view = view;
        this.getStandingsInteractor = getStandingsInteractor;
    }

    @Override
    public void fetchStandingsData() {
        getStandingsInteractor.execute(new GetStandingsInteractor.Callback() {
            @Override
            public void success(StandingsResponse standingsResponse) {
                view.onFetchStandingsDataSuccess(mapStandingsResponseToStandings(standingsResponse));
            }

            @Override
            public void failure(Exception exception) {

            }

            @Override
            public void error(Exception exception) {

            }
        });
    }

    public void dispose() {
        getStandingsInteractor.dispose();
    }

    private List<Standings> mapStandingsResponseToStandings(StandingsResponse standingsResponse) {
        List<StandingsResponse.Ranking> rankingList = standingsResponse.getCompetition().getSeason().getRound().getResults().getRankingList();
        List<Standings> standingsList = new ArrayList<>();

        for (StandingsResponse.Ranking ranking : rankingList) {
            Standings standings = new Standings();
            standings.setClubName(ranking.getClubName());
            standings.setRank(ranking.getRank());
            standings.setMatches(ranking.getMatches());
            standings.setMatchesWon(ranking.getMatchesWon());
            standings.setMatchesDraw(ranking.getMatchesDraw());
            standings.setMatchesLost(ranking.getMatchesLost());
            standings.setPoints(ranking.getPoints());
            standingsList.add(standings);
        }
        return standingsList;
    }

}
