package manka.igor.perform.presentation.ui.news.model;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor

@Root(name = "rss", strict = false)
public class News {

    @Element(name = "channel")
    public Channel channel;

    @Data
    @Root(name = "channel", strict = false)
    public static class Channel {

        @ElementList(name = "item", inline = true, required = false)
        public List<NewsItem> newsItemList;
    }
}
