package manka.igor.perform.presentation.ui.scores.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Scores {

    private String firstTeamName;

    private String secondTeamName;

    private int firstTeamScore;

    private int secondTeamScore;
}
