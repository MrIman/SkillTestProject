package manka.igor.perform.presentation.ui.scores.presenter;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import manka.igor.perform.data.net.entity.ScoresItemResponse;
import manka.igor.perform.data.net.entity.ScoresResponse;
import manka.igor.perform.domain.interactor.scores.GetScoresInteractor;
import manka.igor.perform.presentation.ui.scores.contract.ScoresContract;
import manka.igor.perform.presentation.ui.scores.model.Scores;

public class ScoresPresenter implements ScoresContract.Presenter {

    private ScoresContract.View view;
    private GetScoresInteractor getScoresInteractor;

    @Inject
    public ScoresPresenter(ScoresContract.View view, GetScoresInteractor getScoresInteractor) {

        this.view = view;
        this.getScoresInteractor = getScoresInteractor;
    }

    @Override
    public void fetchScoresData() {
        getScoresInteractor.execute(new GetScoresInteractor.Callback() {
            @Override
            public void success(ScoresResponse scoresResponse) {
                Log.d("success", scoresResponse.toString());
                view.onSuccesGetScores(mapScoresResponseToScores(scoresResponse));
            }

            @Override
            public void failure(Exception exception) {

            }

            @Override
            public void error(Exception exception) {

            }
        });
    }

    private List<Scores> mapScoresResponseToScores(ScoresResponse scoresResponse) {
        List<ScoresResponse.Group> groups = scoresResponse.getCompetition().getSeason().getRound().getGroups();
        List<Scores> scoresList = new ArrayList<>();

        for (int i = 0; i < groups.size(); i++) {
            for (int item = 0; item < groups.get(i).getScoresItemResponseList().size(); item++) {
                Scores scores = new Scores();
                ScoresItemResponse scoreItem = groups.get(i).getScoresItemResponseList().get(item);
                scores.setFirstTeamName(scoreItem.getFirstTeamName());
                scores.setSecondTeamName(scoreItem.getSecondTeamName());
                scores.setFirstTeamScore(scoreItem.getFirstTeamScore());
                scores.setSecondTeamScore(scoreItem.getSecondTeamScore());
                scoresList.add(scores);
            }
        }
        return scoresList;
    }

    public void dispose() {
        getScoresInteractor.dispose();
    }

}
