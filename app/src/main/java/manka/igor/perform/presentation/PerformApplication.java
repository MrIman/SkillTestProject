package manka.igor.perform.presentation;

import android.app.Application;

import lombok.Getter;
import manka.igor.perform.presentation.di.component.ApplicationComponent;
import manka.igor.perform.presentation.di.component.DaggerApplicationComponent;
import manka.igor.perform.presentation.di.component.DaggerDataComponent;
import manka.igor.perform.presentation.di.component.DataComponent;
import manka.igor.perform.presentation.di.module.ApplicationModule;
import manka.igor.perform.presentation.di.module.InteractorModule;
import manka.igor.perform.presentation.di.module.NetModule;


public class PerformApplication extends Application {

    @Getter
    private ApplicationModule applicationModule;
    @Getter
    private ApplicationComponent applicationComponent;
    @Getter
    private DataComponent dataComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        setUpInjection();
    }

    private void setUpInjection() {
        applicationModule = new ApplicationModule(this);

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(applicationModule)
                .build();

        dataComponent = DaggerDataComponent.builder()
                .applicationModule(applicationModule)
                .netModule(new NetModule())
                .interactorModule(new InteractorModule())
                .build();
    }
}
