package manka.igor.perform.presentation.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import manka.igor.perform.data.repository.PerformRepositoryImpl;
import manka.igor.perform.domain.PerformRepository;

@Module
public class RepositoryModule {

    @Provides
    @Singleton
    public PerformRepository providePerformRepository(PerformRepositoryImpl performRepository) {
        return performRepository;
    }
}
