package manka.igor.perform.presentation.ui.standings.di.module;

import dagger.Module;
import dagger.Provides;
import manka.igor.perform.presentation.di.scope.PerActivity;
import manka.igor.perform.presentation.ui.standings.contract.StandingsContract;

@Module
public class StandingsContractModule {

    private StandingsContract.View view;

    public StandingsContractModule(StandingsContract.View view) {

        this.view = view;
    }

    @PerActivity
    @Provides
    public StandingsContract.View providesView() {
        return view;
    }
}
