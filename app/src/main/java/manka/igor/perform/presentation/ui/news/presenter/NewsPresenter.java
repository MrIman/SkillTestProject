package manka.igor.perform.presentation.ui.news.presenter;

import android.util.Log;

import javax.inject.Inject;

import manka.igor.perform.domain.interactor.common.GetPictureInteractor;
import manka.igor.perform.domain.interactor.news.GetNewsInteractor;
import manka.igor.perform.presentation.ui.news.contract.NewsContract;
import manka.igor.perform.presentation.ui.news.model.News;

public class NewsPresenter implements NewsContract.Presenter {

    private NewsContract.View view;
    private GetNewsInteractor getNewsInteractor;
    private GetPictureInteractor getPictureInteractor;

    @Inject
    public NewsPresenter(NewsContract.View view, GetNewsInteractor getNewsInteractor, GetPictureInteractor getPictureInteractor) {

        this.view = view;
        this.getNewsInteractor = getNewsInteractor;
        this.getPictureInteractor = getPictureInteractor;
    }

    public NewsPresenter() {
    }

    @Override
    public void fetchNewsData() {

        getNewsInteractor.execute(new GetNewsInteractor.Callback() {
            @Override
            public void failure(Exception exception) {
                Log.d("Log", "failure");
            }

            @Override
            public void error(Exception exception) {
                Log.d("Log", "error");

            }

            @Override
            public void success(News news) {
                view.onSuccessGetNews(news);
            }
        });
    }

    @Override
    public void fetchPicture(News news) {
        getPictureInteractor.execute(news, new GetPictureInteractor.Callback() {
            @Override
            public void success(News newsWithPicture) {
                view.onSuccessGetPicture(newsWithPicture);
            }

            @Override
            public void failure(Exception exception) {

            }

            @Override
            public void error(Exception exception) {

            }
        });
    }

    public void dispose() {
        getNewsInteractor.dispose();
    }


}
