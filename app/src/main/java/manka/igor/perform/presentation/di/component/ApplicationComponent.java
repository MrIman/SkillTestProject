package manka.igor.perform.presentation.di.component;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Component;
import manka.igor.perform.presentation.di.module.ApplicationModule;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    Context context();
}
