package manka.igor.perform.presentation.ui.scores.di.component;

import dagger.Component;
import manka.igor.perform.presentation.di.component.DataComponent;
import manka.igor.perform.presentation.di.scope.PerActivity;
import manka.igor.perform.presentation.ui.scores.di.module.ScoresContractModule;
import manka.igor.perform.presentation.ui.scores.view.fragment.ScoresFragment;

@PerActivity
@Component(dependencies = DataComponent.class,
        modules = ScoresContractModule.class)
public interface ScoresComponent {

    void inject(ScoresFragment fragment);
}
