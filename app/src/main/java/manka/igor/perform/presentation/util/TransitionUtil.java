package manka.igor.perform.presentation.util;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class TransitionUtil {

    public static void startActivity(Context context, Class<? extends AppCompatActivity> activityClass) {
        startActivity(context, activityClass, null);
    }

    public static void startActivity(Context context, Class<? extends AppCompatActivity> activityClass, Bundle arguments) {
        Intent intent = new Intent(context, activityClass);
        putArgumentsIfExist(intent, arguments);

        context.startActivity(intent);
    }

    private static void putArgumentsIfExist(Intent intent, Bundle arguments) {
        if (arguments != null) {
            intent.putExtras(arguments);
        }
    }


}
