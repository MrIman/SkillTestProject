package manka.igor.perform.presentation.ui.news.view.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.Constants;
import manka.igor.perform.presentation.ui.news.model.News;
import manka.igor.perform.presentation.ui.news.model.NewsItem;
import manka.igor.perform.presentation.ui.webview.activity.WebViewActivity;
import manka.igor.perform.presentation.util.TransitionUtil;

public class NewsRecyclerViewAdapter extends RecyclerView.Adapter<NewsRecyclerViewAdapter.ViewHolder> {

    private static final int LAYOUT_ID = R.layout.news_list_row;
    private News newsList;

    public NewsRecyclerViewAdapter(News newsList) {
        this.newsList = newsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext())
                .inflate(LAYOUT_ID, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final NewsItem newsItem = newsList.getChannel().getNewsItemList().get(position);
        holder.descTextView.setText(newsItem.getDescription());
        holder.dateTextView.setText(newsItem.getPubDate());
        holder.titleTextView.setText(newsItem.getTitle());
        holder.photoImageView.setImageBitmap(newsItem.getBitmap());
        holder.rootLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openWebView(view.getContext(), newsItem.getLink());
            }
        });
    }

    @Override
    public int getItemCount() {
        return newsList.getChannel().getNewsItemList().size();
    }

    private void openWebView(Context context, String link) {
        Bundle arguments = new Bundle();
        arguments.putString(Constants.WEB_VIEW_EXTRA, link);
        TransitionUtil.startActivity(context, WebViewActivity.class, arguments);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_text_view)
        TextView titleTextView;
        @BindView(R.id.date_text_view)
        TextView dateTextView;
        @BindView(R.id.desc_text_view)
        TextView descTextView;
        @BindView(R.id.photo_image_view)
        ImageView photoImageView;

        @BindView(R.id.rootLayout)
        RelativeLayout rootLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
