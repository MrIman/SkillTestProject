package manka.igor.perform.presentation.ui.standings.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.ui.standings.model.Standings;

public class StandingsRecyclerViewAdapter extends RecyclerView.Adapter<StandingsRecyclerViewAdapter.ViewHolder> {

    private static final int LAYOUT_ID = R.layout.standings_list_row;
    private List<Standings> standingsList;

    public StandingsRecyclerViewAdapter(List<Standings> standingsList) {
        this.standingsList = standingsList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(LAYOUT_ID, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Standings standings = standingsList.get(position);
        holder.rank.setText(String.valueOf(standings.getRank()));
        holder.name.setText(String.valueOf(standings.getClubName()));
        holder.matches.setText(String.valueOf(standings.getMatches()));
        holder.matchesWon.setText(String.valueOf(standings.getMatchesWon()));
        holder.matchesLost.setText(String.valueOf(standings.getMatchesLost()));
        holder.matchesDraw.setText(String.valueOf(standings.getMatchesDraw()));
        holder.points.setText(String.valueOf(standings.getPoints()));
    }

    @Override
    public int getItemCount() {
        return standingsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.standingsPosition_textView)
        TextView rank;
        @BindView(R.id.standingsMatches_textView)
        TextView matches;
        @BindView(R.id.standingsMatchesWon_textView)
        TextView matchesWon;
        @BindView(R.id.standingsMatchesLost_textView)
        TextView matchesLost;
        @BindView(R.id.standingsMatchesDraw_textView)
        TextView matchesDraw;
        @BindView(R.id.standingsPoints_textView)
        TextView points;
        @BindView(R.id.standingsName_textView)
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
