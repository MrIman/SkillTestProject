package manka.igor.perform.presentation.ui.main;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.widget.ProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import lombok.Getter;
import manka.igor.perform.R;
import manka.igor.perform.presentation.ui.common.widget.DropdownMenu;
import manka.igor.perform.presentation.ui.news.view.fragment.NewsFragment;

public class PerformActivity extends AppCompatActivity implements DropdownMenu.OnClickListener {

    @BindView(R.id.activityPerform_header)
    DropdownMenu dropdownMenu;

    @Getter
    @BindView(R.id.activityPerform_progressBar)
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perform);
        ButterKnife.bind(this);
        setUpView();
    }

    @Override
    public void onMenuItemClick(Fragment fragment) {
        changeFragment(fragment);
    }

    private void changeFragment(Fragment fragment) {
        if (getSupportFragmentManager().findFragmentByTag(fragment.getClass().getSimpleName()) == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.activityPerform_pageContainer, fragment, fragment.getClass().getSimpleName()).commit();
        }
    }

    private void setUpView() {
        changeFragment(new NewsFragment());
        dropdownMenu.setOnMenuItemClickListener(this);
    }

}
