package manka.igor.perform.presentation.ui.scores.contract;

import java.util.List;

import manka.igor.perform.presentation.ui.scores.model.Scores;

public interface ScoresContract {

    interface View {

        void onSuccesGetScores(List<Scores> scoresList);
    }

    interface Presenter {

        void fetchScoresData();
    }
}
