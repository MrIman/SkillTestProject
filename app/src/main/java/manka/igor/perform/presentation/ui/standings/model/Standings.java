package manka.igor.perform.presentation.ui.standings.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Standings {

    int rank;

    String clubName;

    int matches;

    int matchesWon;

    int matchesDraw;

    int matchesLost;

    int points;
}
