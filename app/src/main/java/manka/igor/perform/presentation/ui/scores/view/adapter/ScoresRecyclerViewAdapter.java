package manka.igor.perform.presentation.ui.scores.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.ui.scores.model.Scores;

public class ScoresRecyclerViewAdapter extends RecyclerView.Adapter<ScoresRecyclerViewAdapter.ViewHolder> {

    private static final int LAYOUT_ID = R.layout.scores_list_row;
    private List<Scores> scoresList;

    public ScoresRecyclerViewAdapter(List<Scores> scoresList) {
        this.scoresList = scoresList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View rowView = LayoutInflater.from(parent.getContext())
                .inflate(LAYOUT_ID, parent, false);
        return new ViewHolder(rowView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Scores scores = scoresList.get(position);
        holder.scoresTextView.setText(new StringBuilder()
                .append(scores.getFirstTeamScore())
                .append(" - ")
                .append(scores.getSecondTeamScore()));
        holder.firstTeamTextView.setText(scores.getFirstTeamName());
        holder.secondTeamTextView.setText(scores.getSecondTeamName());
    }

    @Override
    public int getItemCount() {
        return scoresList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.scores_textView)
        TextView scoresTextView;
        @BindView(R.id.firstTeam_textView)
        TextView firstTeamTextView;
        @BindView(R.id.secondTeam_textView)
        TextView secondTeamTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
