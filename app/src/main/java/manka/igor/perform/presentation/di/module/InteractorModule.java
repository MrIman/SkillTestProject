package manka.igor.perform.presentation.di.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import manka.igor.perform.domain.interactor.common.GetPictureInteractor;
import manka.igor.perform.domain.interactor.common.impl.GetPictureInteractorImpl;
import manka.igor.perform.domain.interactor.news.GetNewsInteractor;
import manka.igor.perform.domain.interactor.news.impl.GetNewsInteractorImpl;
import manka.igor.perform.domain.interactor.scores.GetScoresInteractor;
import manka.igor.perform.domain.interactor.scores.impl.GetScoresInteractorImpl;
import manka.igor.perform.domain.interactor.standings.GetStandingsInteractor;
import manka.igor.perform.domain.interactor.standings.impl.GetStandingsInteractorImpl;

@Module
public class InteractorModule {

    @Provides
    @Singleton
    public GetNewsInteractor provideGetNewsInteractor(GetNewsInteractorImpl getNewsInteractor) {
        return getNewsInteractor;
    }

    @Provides
    @Singleton
    public GetPictureInteractor provideGetPictureInteractor(GetPictureInteractorImpl getPictureInteractor) {
        return getPictureInteractor;
    }

    @Provides
    @Singleton
    public GetScoresInteractor provideGetScoresInteractor(GetScoresInteractorImpl getScoresInteractor) {
        return getScoresInteractor;
    }

    @Provides
    @Singleton
    public GetStandingsInteractor provideGetStandingsInteractor(GetStandingsInteractorImpl getStandingsInteractor) {
        return getStandingsInteractor;
    }
}
