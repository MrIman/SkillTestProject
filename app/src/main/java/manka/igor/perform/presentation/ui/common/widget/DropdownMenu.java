package manka.igor.perform.presentation.ui.common.widget;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import lombok.Setter;
import manka.igor.perform.R;
import manka.igor.perform.presentation.ui.news.view.fragment.NewsFragment;
import manka.igor.perform.presentation.ui.scores.view.fragment.ScoresFragment;
import manka.igor.perform.presentation.ui.standings.view.fragment.StandingsFragment;
import manka.igor.perform.presentation.util.TransformationUtil;

public class DropdownMenu extends RelativeLayout {

    private static final int LAYOUT_ID = R.layout.dropdown_menu;
    private static final String[] menuListItems = {"NEWS", "SCORES", "STANDINGS"};

    @Setter
    OnClickListener onMenuItemClickListener;

    @BindView(R.id.menu_listView)
    ListView menuListView;
    @BindView(R.id.menu_button)
    ImageButton menuButton;

    public DropdownMenu(Context context) {
        super(context);
    }

    public DropdownMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public DropdownMenu(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs, defStyleAttr);
    }

    @OnClick(R.id.menu_button)
    void onMenuButtonClick() {
        changeMenuState();
    }

    private void init(AttributeSet attrs, int defStyleAttr) {
        initAttrs(attrs, defStyleAttr);
    }

    private void initAttrs(AttributeSet attrs, int defStyleAttr) {
        inflate(getContext(), LAYOUT_ID, this);
        ButterKnife.bind(this);
        initList();
    }

    private void initList() {
        ArrayList<String> list = new ArrayList<>();
        list.addAll(Arrays.asList(menuListItems));
        menuListView.setAdapter(new ArrayAdapter<String>(getContext(), R.layout.menu_list_row, list));
        menuListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (onMenuItemClickListener != null) {
                    switch (i) {
                        case 0:
                            onMenuItemClickListener.onMenuItemClick(new NewsFragment());
                            break;
                        case 1:
                            onMenuItemClickListener.onMenuItemClick(new ScoresFragment());
                            break;
                        case 2:
                            onMenuItemClickListener.onMenuItemClick(new StandingsFragment());
                    }
                    changeMenuState();
                }
            }
        });
    }

    private void changeMenuState() {
        if (menuListView.getVisibility() == VISIBLE) {
            TransformationUtil.rotateView(menuButton, 180, 0);
            menuListView.setVisibility(GONE);
        } else {
            TransformationUtil.rotateView(menuButton, 0, 180);
            menuListView.setVisibility(VISIBLE);
        }

    }

    public interface OnClickListener {

        void onMenuItemClick(Fragment fragment);
    }
}
