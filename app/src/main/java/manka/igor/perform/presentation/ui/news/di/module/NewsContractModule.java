package manka.igor.perform.presentation.ui.news.di.module;

import dagger.Module;
import dagger.Provides;
import manka.igor.perform.presentation.di.scope.PerActivity;
import manka.igor.perform.presentation.ui.news.contract.NewsContract;

@Module
public class NewsContractModule {

    private NewsContract.View view;

    public NewsContractModule(NewsContract.View view) {
        this.view = view;
    }

    @Provides
    @PerActivity
    public NewsContract.View provideView() {
        return view;
    }
}
