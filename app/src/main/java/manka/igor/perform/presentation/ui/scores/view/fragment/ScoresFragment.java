package manka.igor.perform.presentation.ui.scores.view.fragment;


import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import manka.igor.perform.R;
import manka.igor.perform.presentation.PerformApplication;
import manka.igor.perform.presentation.ui.base.fragment.BaseFragment;
import manka.igor.perform.presentation.ui.main.PerformActivity;
import manka.igor.perform.presentation.ui.scores.contract.ScoresContract;
import manka.igor.perform.presentation.ui.scores.di.component.DaggerScoresComponent;
import manka.igor.perform.presentation.ui.scores.di.module.ScoresContractModule;
import manka.igor.perform.presentation.ui.scores.model.Scores;
import manka.igor.perform.presentation.ui.scores.presenter.ScoresPresenter;
import manka.igor.perform.presentation.ui.scores.view.adapter.ScoresRecyclerViewAdapter;

public class ScoresFragment extends BaseFragment implements ScoresContract.View {

    public static final int layout_id = R.layout.fragment_scores;

    @Inject
    ScoresPresenter scoresPresenter;

    @BindView(R.id.scores_recyclerView)
    RecyclerView scoresRecyclerView;

    private ScoresRecyclerViewAdapter scoresRecyclerViewAdapter;

    public ScoresFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(layout_id, container, false);
        ButterKnife.bind(this, view);
        setUpInjection();
        setUpView();
        return view;

    }

    @Override
    public void onResume() {
        super.onResume();
        setUpView();
    }

    @Override
    public void onPause() {
        super.onPause();
        scoresPresenter.dispose();
    }

    @Override
    public void setUpInjection() {
        DaggerScoresComponent.builder()
                .dataComponent(((PerformApplication) getActivity().getApplication()).getDataComponent())
                .scoresContractModule(new ScoresContractModule(this))
                .build()
                .inject(this);
    }

    @Override
    public void onSuccesGetScores(List<Scores> scoresList) {
        setUpRecyclerView(scoresList);
        try {
            ((PerformActivity) getActivity()).getProgressBar().setVisibility(View.GONE);
        } catch (Exception ex) {
            Log.d("Error", ex.getMessage());
        }
    }

    private void setUpView() {
        ((PerformActivity) getActivity()).getProgressBar().setVisibility(View.VISIBLE);
        scoresPresenter.fetchScoresData();
    }

    private void setUpRecyclerView(List<Scores> scoresList) {
        scoresRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        scoresRecyclerViewAdapter = new ScoresRecyclerViewAdapter(scoresList);
        scoresRecyclerView.setAdapter(scoresRecyclerViewAdapter);
        scoresRecyclerViewAdapter.notifyDataSetChanged();
    }
}
